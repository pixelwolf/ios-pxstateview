//
//  ViewController.swift
//  Example
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

import UIKit
import PxStateView

class ViewController: UIViewController {
    
    static var defaultTitleStyle: [NSAttributedString.Key: Any]? = [
        NSAttributedString.Key.foregroundColor: UIColor.black,
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 28, weight: .semibold)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        view.backgroundColor = .black
    }

    private func setupView() {
        var elements: [PxStateViewElement] = []
        
        for i in 0...30 {
            elements.append(makeTextElement())
        }
        
        let representable = DefaultPxStateRepresentable(elements: elements)

        let stateView = PxStateView(with: representable)
        view.addSubview(stateView)
        
        NSLayoutConstraint.activate([
            stateView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100),
            stateView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            stateView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            stateView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
            ])
    }
    
    func makeTextElement() -> PxStateViewElement {
        let textConstraints = PxStateViewElementConstraints(top: 8.0, leading: 16.0, trailing: 16.0, bottom: 8.0, height: nil)
        
        let textElement = PxStateViewElement(text: "HAHAHAHAHAHAHAHAHAHHAHAHAHHAHAHAHAHAHAHHA", style: ViewController.defaultTitleStyle, constraints: textConstraints)
        return textElement
    }
}
