Pod::Spec.new do |spec|
  spec.name             = 'PxStateView'
  spec.version          = '0.0.1'
  spec.summary          = 'PixelJukebox is an iOS audio player written in Swift'
  spec.homepage         = 'https://bitbucket.org/pixelwolf/ios-pxstateview'
  spec.license          = { :type => 'MIT', :file => 'LICENSE' }
  spec.author           = { 'Orlando Amorim' => 'orlando.filho95@icloud.com' }
  spec.source           = { :git => 'https://bitbucket.org/pixelwolf/ios-pxstateview.git', :tag => spec.version.to_s }
  spec.social_media_url = 'https://twitter.com/or_amorim'
  spec.ios.deployment_target = '9.0'
  spec.source_files = 'PxStateView/*.swift'
  spec.frameworks = 'UIKit'
  spec.swift_version = '5.0'
end
