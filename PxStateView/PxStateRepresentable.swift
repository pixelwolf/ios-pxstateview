//
//  PxStateRepresentable.swift
//  PxStateView
//
//  Created by Orlando Amorim on 12/06/19.
//  Copyright © 2019 Pixelwolf. All rights reserved.
//

public protocol PxStateRepresentable {
    var elements: [PxStateViewElement] { get set }
    var configuration: PxStateViewConfiguration { get set }
}
